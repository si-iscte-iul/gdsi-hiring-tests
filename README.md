# GDSI hiring exercises #

This repository is dedicated to hiring exercises for candidates in the hiring context for ISCTE's information systems development office (GDSI).

## What is this repository for? ##

This repository is for candidates in any hiring process with GDSI. The candidate should clone this repository and then complete the proposed exercises (as provided in the hiring process) by submitting a pull request.

## Who do I talk to? ##

If any questions arise, please send them to this e-mail: [coordenador.gdsi@iscte-iul.pt](mailto:coordenador.gdsi@iscte-iul.pt).
